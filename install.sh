#!/bin/bash

if ! hash git 2>/dev/null; then
  echo >&2 "Git is required for this tool to work. Aborting."
  exit 1
fi

if [ "$DOT_FILES_DIR" == "" ]; then
  DOT_FILES_DIR=~/.dot-files
fi

if [ "$DOT_FILES_SKIP_SSH" == "true" ]; then
  git clone https://gitlab.com/ThunderKey/dot-files.git "$DOT_FILES_DIR"
else
  git clone git@gitlab.com:ThunderKey/dot-files.git "$DOT_FILES_DIR"
fi

if ! [ -d "$DOT_FILES_DIR" ] || ! [ -e "$DOT_FILES_DIR/setup_dot_files.sh" ]; then
  echo "Clone of the repo failed!"
  exit 1
fi

exitstatus=0

pushd $DOT_FILES_DIR > /dev/null || exit

git submodule update --init

./setup_dot_files.sh "$@"

mapfile -d $'\0' scripts < <(find scripts/update -type f -print0)
for script in "${scripts[@]}"; do
  if ! eval "$script"; then
    echo "$script failed to execute"
    exitstatus=3
  fi
done

popd > /dev/null || exit
exit $exitstatus
