#!/bin/bash

dot_files_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd "$dot_files_dir" > /dev/null || exit

mapfile -d $'\0' files < <(find files -type f -print0)

diff-files() {
  local sourceFile="$1"
  local targetFile="$2"
  if [[ "$targetFile" == "files/.config/kde.org/ghostwriter.conf" ]]; then
    exclude-lines() {
      grep -vE '^(mainWindowGeometry|mainWindowState|splitterGeometry)=' "$1"
    }
    diff -q <(exclude-lines "$sourceFile") <(exclude-lines "$targetFile")
    return $?
  fi
  diff -q "$sourceFile" "$targetFile"
  return $?
}

for file in "${files[@]}"; do
  file="${file//files\//}"
  if ! diff-files "$HOME/$file" "files/$file"; then
    nvim -d "$HOME/$file" "files/$file"
  fi
done

popd > /dev/null || exit
