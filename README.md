*Important*: Superceded by https://gitlab.com/ThunderKey/home-manager-flake

The is a collection of my own dot-files and tools to install them automatically.

## Installation

To install the repository and all dot-files you can simply execute the following command:

```shell
bash -c "$(wget https://gitlab.com/ThunderKey/dot-files/raw/master/install.sh -O -)"
```

Or without ssh:

```shell
DOT_FILES_SKIP_SSH=true bash -c "$(wget https://gitlab.com/ThunderKey/dot-files/raw/master/install.sh -O -)"
```

This script clones the git repository to either the value of the bash variable `$DOT_FILES_DIR` or if not set to `~/.dot-files`. After the clone it will call the script `setup_dot_files.sh` within the cloned repository in the interactive mode.

## Dot-Files setup

The script `setup_dot_files.sh` goes through all files in the `files` directory and copies them to the home directory if they don't exist there. There are three options if they already exist:
* If the no parameter is given it will use the interactive mode. This means it displays the diff and asks if it should be overwritten or not.
* If the force parameter (--force|-f) is given it will overwrite all files.
* If the safe parameter (--safe|-s) is given it only displays the diff but does not overwrite the files. This is useful for example in cronjobs.

The scripts `install.sh` and `update.sh` pass all arguments to the setup script. This means that `./update.sh --safe` will update the git repository and call the `setup_dot_files.sh` script in the safe mode.

## Variables

For the scripts `install.sh`, `update.sh` and `setup_dot_files.sh` you can set the bash variable `$HOME_DIR` which default is set to `~`.
