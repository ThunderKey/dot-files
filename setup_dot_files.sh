#!/bin/bash

display_help() {
  script=$(basename "$0")
  echo "$0 copies all the dot fiels to the home directory."
  echo "Usage:"
  echo "  $script [--verbose|-v]              - If a dot file already exists with different content ask what todo"
  echo "  $script [--verbose|-v] --force|-f   - If a dot file already exists with different content overwrite it"
  echo "  $script [--verbose|-v] --safe|-s    - If a dot file already exists with different content only display the diff but do nothing"
  echo "  $script --help|-h                   - Display this message"
}

# read args
force=false
safe=false
verbose=false
while test $# -gt 0; do
  case "$1" in
    --force|-f) force=true
      ;;
    --verbose|-v) verbose=true
      ;;
    --safe|-s) safe=true
      ;;
    --help|-h) display_help; exit
      ;;
    *) echo "unknown option \"$1\""; display_help; exit 2
      ;;
  esac
  shift
done

if $force && $safe; then
  echo >&2 "the force and safe flags cannot be used at the same time. Aborting."
  display_help
  exit 1
fi


replace_files() {
  test "$REPLACE_FILES" == "true"
  return $?
}

ask() {
  while true; do
    read -r -p "$1" response
    response=${response,,} # tolower
    case "$response" in
      y|ye|yes) exit 0
        ;;
      n|no) exit 1
        ;;
    esac
    echo "Could not get your answer. Please use \"yes\", \"y\", \"no\" or \"n\"."
  done
}

[[ "$HOME_DIR" == "" ]] && HOME_DIR=~

dot_files_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$PREVIOUS_GIT_COMMIT" == "" ]; then
  PREVIOUS_GIT_COMMIT=$(cat "$dot_files_dir/.last_successful_setup" 2>/dev/null)
fi

success=true

mapfile -d $'\0' files < <(find "$dot_files_dir/files" -type f,l -print0)
for file in "${files[@]}"; do
  file=${file//$dot_files_dir\/files\//}
  new_file=$HOME_DIR/$file
  original_file="$dot_files_dir/files/$file"
  copy=false
  if [ -e "$new_file" ]; then
    if cmp -s "$original_file" "$new_file"; then
      $verbose && echo "$file" did not change
    elif [ "$PREVIOUS_GIT_COMMIT" != "" ] && git show "$PREVIOUS_GIT_COMMIT:files/$file" &>/dev/null && cmp -s <(git show "$PREVIOUS_GIT_COMMIT:files/$file") "$new_file"; then
      copy=true
    else
      if $verbose || ! $force; then
        if hash git 2>/dev/null; then
          git diff "$new_file" "$original_file"
        else
          diff "$new_file" "$original_file"
        fi
      fi

      if ! $safe && ($force || ask "Overwrite the file \"$file\"? (yes|no) "); then
        copy=true
      else
        success=false
      fi
    fi
  else
    copy=true
  fi

  if $copy; then
    $verbose && echo "copy $original_file to $new_file"
    mkdir -p "$(dirname "$new_file")"
    cp -d "$original_file" "$new_file"
  fi
done

if $success; then
  git rev-parse HEAD > "$dot_files_dir/.last_successful_setup"
fi
