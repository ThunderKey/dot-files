#!/bin/bash

set -euo pipefail

nvim +"lua require('lazy').sync({wait=true})" +qall
