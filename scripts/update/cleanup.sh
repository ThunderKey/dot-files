#!/bin/bash

set -e

[[ "$HOME_DIR" == "" ]] && HOME_DIR=~

# Ensure the following files are removed
for file in "$HOME/.config/tmuxinator/docker.yml" "$HOME/.tmux.conf"; do
  [[ -e "$file" ]] && echo "removing $file" && rm "$file"
done

echo finished cleaning up
