#!/bin/env fish

function __ensure_user_service_running -a name
    if not systemctl --user is-enabled "$name" > /dev/null
        systemctl --user enable "$name"
        systemctl --user start "$name"

        if not systemctl --user is-enabled "$name" > /dev/null
            echo "Could not enable service $name"
            exit 1
        end
    end
end

__ensure_user_service_running battery-notification
