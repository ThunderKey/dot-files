#!/bin/bash

set -e

scripts_dir="$(cd "$(dirname "$(dirname "${BASH_SOURCE[0]}")")" && pwd)"

. "$scripts_dir/helpers/tools.sh"

if ! hash fish 2> /dev/null; then
  echo_warning "If you want to use fish, please install it and call \"$scripts_dir/update/fisher.sh\""
  exit 1
elif ! fish -ic 'type -q fisher'; then
  echo_warning "If you want to use fisher, please install it and call \"$scripts_dir/update/fisher.sh\""
  exit 2
fi

fish -ic 'fisher update'
