#!/bin/bash

echo_error() {
  echo -e "\e[1;31m$1\e[m" >&2
}

echo_warning() {
  echo -e "\e[1;33m$1\e[m" >&2
}
