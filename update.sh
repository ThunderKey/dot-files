#!/bin/bash

if ! hash git 2>/dev/null; then
  echo >&2 "Git is required for this tool to work. Aborting."
  exit 1
fi

if [[ "$1" == "full" ]]; then
  export FULL_UPDATE=true
  shift
elif [[ "$1" != "" ]]; then
  echo "Unknown options: \"$*\""
  echo "Usage $0 [full]"
  exit 1
else
  export FULL_UPDATE=false
fi

dot_files_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd "$dot_files_dir" > /dev/null || exit

exitstatus=0

if ! git pull origin master; then
  echo "could not update the repo"
  exit 2
fi

if [[ "$FULL_UPDATE" == "true" ]]; then
  if ! git submodule update --remote --init --recursive; then
    echo "could not update the git submodules from the remote"
    exit 2
  fi
else
  if ! git submodule update --init --recursive; then
    echo "could not update the git submodules"
    exit 2
  fi
fi

./setup_dot_files.sh "$@"

mapfile -d $'\0' scripts < <(find scripts/update -type f -print0)
for script in "${scripts[@]}"; do
  if ! eval "$script"; then
    echo "$script failed to execute"
    exitstatus=3
  fi
done

popd > /dev/null || exit
exit $exitstatus
