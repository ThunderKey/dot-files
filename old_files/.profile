# configuration for NNN:
# detail & open text files in editor & show file information
export NNN_OPTS="di"
# hidden files on top
export LC_COLLATE="C"
# temporary buffer for the previews
export NNN_FIFO="/tmp/nnn.fifo"
# many other plugins are available here: https://github.com/jarun/nnn/tree/master/plugins
export NNN_PLUG="p:preview-tui;d:dragdrop"
# use gio trash
export NNN_TRASH=2

# load custom profile
[ -f "$HOME/.profile_custom" ] && source "$HOME/.profile_custom"
