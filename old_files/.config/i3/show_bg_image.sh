#!/bin/bash

set -euo pipefail

CONFIG_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)"

feh --bg-max --image-bg black "$CONFIG_DIR/eye_of_the_universe.png"
