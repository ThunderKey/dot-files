#!/bin/bash

set -e

CONFIG_DIR="$HOME/.config/i3"

[[ -f $CONFIG_DIR/workspace-10.json ]] && i3-msg "workspace 10: ; append_layout $CONFIG_DIR/workspace-10.json"
[[ -f $CONFIG_DIR/workspace-2.json ]] && i3-msg "workspace 2: 󰭹; append_layout $CONFIG_DIR/workspace-2.json"
[[ -f $CONFIG_DIR/workspace-1.json ]] && i3-msg "workspace 1: 󰖟; append_layout $CONFIG_DIR/workspace-1.json"

[[ -f $CONFIG_DIR/default_applications.sh ]] && $CONFIG_DIR/default_applications.sh
