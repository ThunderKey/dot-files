#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do
  killall -q polybar
  sleep 1
done

IFS=$'\n'  # only split on newline
for entry in $(polybar --list-monitors); do
  monitor="$(echo "$entry" | cut -d":" -f1)"
  if echo "$entry" | grep '(primary)'; then
    tray_pos='right'
  else
    tray_pos=""
  fi

  MONITOR=$monitor TRAY_POS=$tray_pos polybar --reload topbar &
done
unset IFS  # reset IFS
