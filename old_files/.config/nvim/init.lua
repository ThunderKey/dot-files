-- set runtimepath^=~/.vim runtimepath+=~/.vim/after
-- let &packpath = &runtimepath
-- source ~/.vimrc

-- set the leader to space
-- overrides original behaviour: same as l => move right)
vim.g.mapleader = " "

require("plugins")
require("basic_settings")
require("keys")
require("highlighting")
require("hooks")
require("filetypes")
