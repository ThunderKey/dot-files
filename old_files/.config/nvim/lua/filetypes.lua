vim.filetype.add({
  extension = {
    prawn = "ruby",
    hbs = "html",
    twig = "html",
    nunjucks = "html",
  },
})

vim.g.tex_flavor = "latex"
