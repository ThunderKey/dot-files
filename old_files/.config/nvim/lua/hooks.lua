-- highlight what was yanked
vim.api.nvim_create_autocmd("TextYankPost", {
  callback = function()
    vim.highlight.on_yank({ higroup = "IncSearch", timeout = 150 })
  end
})

vim.api.nvim_create_autocmd("FileType", {
  callback = function()
    vim.opt.formatoptions:remove("c") -- c: enable auto-wrap comments
    vim.opt.formatoptions:remove("r") -- r: insert comments after <Enter> in insert mode
    vim.opt.formatoptions:remove("o") -- o: insert comments after o or O in normal mode
  end
})

-- remember line it was on the last time the file was open
vim.api.nvim_create_autocmd("BufReadPost", {
  callback = function()
    local row, column = unpack(vim.api.nvim_buf_get_mark(0, '"'))
    local buf_line_count = vim.api.nvim_buf_line_count(0)

    if row >= 1 and row <= buf_line_count then
      vim.api.nvim_win_set_cursor(0, { row, column })
    end
  end
})

vim.api.nvim_create_autocmd({"BufRead", "BufNewFile"}, {
  pattern = vim.fn.expand("~/.dot-files/files/.config/nvim/lazy-lock.json"),
  callback = function()
    vim.o.fixendofline = false
  end
})

-- enable spell checking
-- ]s / [s => move to misspelled word
-- z= => suggest alternatives
-- zg => add to dictionary
-- zw => mark as incorrect
local EnableSpellchecking = function()
  vim.o.spell = true
  vim.o.spelllang = "en_gb"
end
vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
  callback = EnableSpellchecking,
  pattern = { "*tex", "*.md" },
})
