-- 2 spaces instead of tab
vim.o.expandtab = true
vim.o.tabstop = 8 -- show \t as 8 spaces
vim.o.softtabstop = 2 -- when pressing <tab> insert 2 spaces
vim.o.shiftwidth = 2

-- color scheme which is better in dark terminals
vim.o.background = "dark"

-- add a column that indicates 100 characters
vim.o.colorcolumn = "100"

-- dont show the cursorline in the diff mode because there are issues with the black background
if not vim.api.nvim_win_get_option(0, "diff") then
  vim.o.cursorline = true
end

-- enable true colors
if vim.fn.exists('+termguicolors') then
  vim.o.termguicolors = true
else
  vim.print("true colors not available")
end

-- filetype plugin on
-- vim.omnifunc=syntaxcomplete#Complete

-- highlight matching [{()}]
vim.o.showmatch = true

-- highlight matches
vim.o.hlsearch = true

-- all lowercase: insensitve; otherwise: sensitive
vim.o.ignorecase = true
vim.o.smartcase = true

-- always try to have 5 lines below the current line
vim.o.scrolloff = 5

vim.o.encoding = "utf-8"

vim.o.foldmethod = "marker"
