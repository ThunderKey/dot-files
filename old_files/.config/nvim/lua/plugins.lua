local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  "rhysd/vim-crystal",

  "leafgarland/typescript-vim",

  "cespare/vim-toml",

  {
    "ThunderKey/vim-material-monokai",
    config = function()
      vim.cmd.colorscheme("material-monokai")
      vim.g.materialmonokai_italic = 1
    end,
  },

  {
    "vim-airline/vim-airline",
    init = function()
      vim.g.airline_theme = "materialmonokai"
      vim.g.airline_powerline_fonts = 1 -- requires fonts-powerline
    end,
  },

  {
    "lervag/vimtex",
    init = function()
      vim.g.vimtex_view_method = "zathura"
    end,
  },

  -- ]q = :cnext; [q = :cprevious; ]a = :next; [b = :bprevious
  -- [/]{Space} add newline before/after
  -- ]e change the currentline with the previous
  "tpope/vim-unimpaired",

  -- interpret line numbers after the filename (e.g. vim index.html:20)
  "bogado/file-line",

  -- eg: cs"' to replace surrounding " with '
  "tpope/vim-surround",

  -- eg: griw replacy inner word with current register without copying the word
  "vim-scripts/ReplaceWithRegister",

  -- enable plugins to repeat commands
  "tpope/vim-repeat",

  {
    "vim-syntastic/syntastic",
    init = function()
      vim.g.syntastic_always_populate_loc_list = 1
      vim.g.syntastic_auto_loc_list = 1
      vim.g.syntastic_check_on_open = 1
      vim.g.syntastic_check_on_wq = 1
      vim.g.syntastic_aggregate_error = 1

      vim.g.syntastic_tex_checkers = { "chktex" }

      vim.g.syntastic_java_checkers = { "javac" }
      vim.g.syntastic_java_javac_config_file_enabled = 1

      -- set the nvim host prog variable to a custom pyhton env
      -- Installation:
      --   virtualenv ~/.vim/python_env
      --   . ~/.vim/python_env/bin/activate
      --   pip install pynvim
      -- Check with :checkhealth
      vim.g.python3_host_prog = "~/.vim/python_env/bin/python"
    end,
  },

  -- git diff information in files
  {
    "airblade/vim-gitgutter",
    init = function()
      -- let g:gitgutter_highlight_linenrs = 1
      -- let g:gitgutter_use_location_list = 1
      -- set updatetime = 100
      -- highlight link GitGutterChangeLineNr Underlined
      -- function! GitStatus()
      --   let [a,m,r] = GitGutterGetHunkSummary()
      --   return printf("+%d ~%d -%d", a, m, r)
      -- endfunction
      -- set statusline += %{GitStatus()}
    end,
  },

  -- git diff information in files
  {
    "SirVer/ultisnips",
    init = function()
      -- Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
      -- - https://github.com/Valloric/YouCompleteMe
      -- - https://github.com/nvim-lua/completion-nvim
      vim.g.UltiSnipsExpandTrigger = "<c-space>"
      vim.g.UltiSnipsJumpForwardTrigger = "<c-j>"
      vim.g.UltiSnipsJumpBackwardTrigger = "<c-k>"
      -- Disable autotrigger (https://github.com/SirVer/ultisnips/issues/593)
      vim.cmd([[
        augroup ultisnips_no_auto_expansion
          au!
          au VimEnter * au! UltiSnips_AutoTrigger
        augroup END
      ]])
    end,
  },

  -- Predefined snippets
  "honza/vim-snippets",

  -- syntax
  "khaveesh/vim-fish-syntax",
  "LnL7/vim-nix",
})
