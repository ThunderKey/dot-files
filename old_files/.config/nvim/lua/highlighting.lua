-- highlight whitespace at the end of the line red
vim.api.nvim_set_hl(0, "ExtraWhitespace", { bg = "Red" })
vim.fn.matchadd("ExtraWhitespace", "\\s\\+$")

-- highlight tabs yellow
vim.api.nvim_set_hl(0, "Tabs", { bg = "Yellow" })
-- vim.cmd.highlight(1, "Tabs", "ctermbg=Yellow guibg=Yellow")
vim.fn.matchadd("Tabs", "\\t")
