-- function to toggle mouse support
-- map ToggleMouse to F2
vim.keymap.set({"n", "v", "i"}, "<f2>", function()
  error("Functionality does not work anymore! Use Shift to disable")
end)
-- default enable mouse
vim.o.mouse = "a"

local SwitchLineNumbers = function()
  vim.o.number = not vim.o.number
  vim.o.relativenumber = not vim.o.relativenumber
end
SwitchLineNumbers()

-- set/unset switch through relative, absolute and no line numbers F7
vim.keymap.set({"n", "v", "i"}, "<f7>", SwitchLineNumbers)

vim.keymap.set("n", "<f12>", function()
  -- remove tabs
  vim.cmd("silent! %s/\\t/        /g")

  -- remove trailing spaces
  vim.cmd("silent! %s/  *$//g")
end)

-- use fd as escape
vim.keymap.set("i", "fd", "<Esc>")
