#!/bin/bash

set -euo pipefail

dot_files_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd "$dot_files_dir" > /dev/null || exit

mapfile -d $'\0' old_files < <(find old_files -type f -print0)

if [[ "$*" == *"-v"* ]]; then
  verbose=true
else
  verbose=false
fi

function try_remove() {
  targetFile="$HOME/$1"
  if ! test -e "$targetFile"; then
    return
  fi

  if [[ -L "$targetFile" ]] && [[ "$(readlink -f "$targetFile")" = "/nix/"* ]]; then
    # ignore if managed by nix
    return
  fi

  sourceFile="$dot_files_dir/old_files/$1"
  if ! diff -q "$sourceFile" "$targetFile" > /dev/null; then
    echo "NOT REMOVING $targetFile, because it differs from $sourceFile"
    if [[ "$verbose" = true ]]; then
      echo
      diff "$sourceFile" "$targetFile" || true
      echo
    fi
    return
  fi

  echo "Removing $targetFile"
  rm "$targetFile"
}

for file in "${old_files[@]}"; do
  file="${file//old_files\//}"
  try_remove "$file"
done

popd > /dev/null || exit
